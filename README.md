fabric8-devops
==============

A repository of devops examples for automatically provisioning and testing fabrics on different infrastructure

The following folders may be of interest:

* [autoscaler](autoscaler) for various [fabric8 autoscaler](http://fabric8.io/gitbook/requirements.html) demo templates
* [openstack](openstack) for [OpenStack](https://www.openstack.org/) related scripts
* [openshift](openshift) for [OpenShift](http://openshift.com/) related scripts
* [vagrant](vagrant) for [vagrant](http://www.vagrantup.com/) related scripts

